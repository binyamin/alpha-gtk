# -*- coding: utf-8 -*-

from pprint import pprint

from gi.repository import Gtk, GdkPixbuf

from mutagen.mp3 import MP3
from mutagen.easyid3 import EasyID3
import mutagen

@Gtk.Template(resource_path='/com/binyamingreen/gtkalpha/window.ui')
class AlphaWindow(Gtk.ApplicationWindow):
    __gtype_name__ = 'AlphaWindow'

    content = Gtk.Template.Child()
    file_input = Gtk.Template.Child()
    media = Gtk.Template.Child()
    liststoreValues = Gtk.Template.Child()

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        
        self.file_input.connect('file-set', self.on_file)
        self._show_tags('/home/binyamin/Music/library/(Holst) Charles Dutoit/The Planets/01 - Holst (Charles Dutoit) - Mars, the Bringer of War.mp3')

    def on_file(self, target):
        f = target.get_filename()
        self._show_tags(f)

    def _show_tags(self, filepath):
        audio_data = MP3(filepath, ID3=EasyID3)

        self.liststoreValues.clear()

        for (k,v) in audio_data.items():
            self.liststoreValues.append([k, v[0]])

        audio_data = MP3(filepath)

        p = buffer_to_pixbuf(audio_data.get("APIC:").data)
        p = scale_pixbuf(p, 128)
        self.media.set_from_pixbuf(p)

def buffer_to_pixbuf(buffer):
    loader = GdkPixbuf.PixbufLoader()
    loader.write(buffer)
    loader.close()
    return loader.get_pixbuf()

def scale_pixbuf(pixbuf, dest_width):
    return pixbuf.scale_simple(
        dest_width,
        (pixbuf.get_height() / pixbuf.get_width()) * dest_width,
        GdkPixbuf.InterpType.BILINEAR
    )
