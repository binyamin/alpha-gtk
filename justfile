set dotenv-load := false
set positional-arguments := true

_builddir := "build"

alias config := configure

_default:
  @just --list

# Configure or reset meson
configure:
    [ -d {{_builddir}} ] && rm -rf {{_builddir}}
    meson setup {{_builddir}}
    meson configure {{_builddir}} --prefix="$(pwd)/out"

# Compile
build:
    cd {{_builddir}} && meson install
    chmod u+x out/bin/alpha

run:
    ./out/bin/alpha

start:
    @just build > /dev/null
    @just run

dev:
    #!/usr/bin/env bash
    export GTK_DEBUG="interactive"
    just start

# Just a placeholder recipe
test:
    @echo "No test specified"